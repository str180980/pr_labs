var net = require('net');
const readline = require('readline');
const repl = require('repl');
var client = new net.Socket();


client.connect(1337, '127.0.0.1', () => {
  console.log('Connected');
  //client.write('data');
});

client.on('data', data => {
  console.log('Received:\n ' + data);
  rl.setPrompt(prefix, prefix.length);
  rl.prompt();
  //replServer.displayPrompt();
  //client.end(); // kill client after server's response
});

client.on('close', () => {
  console.log('Connection closed');
});

rl = readline.createInterface(process.stdin, process.stdout),
  prefix = '$tcpClient: ';

rl.on('line', line => {

  let testReg = /^[a-zA-Z0-9/_\s]*$/.test(line);
  if(!testReg) {
    console.log(">> Wrong command format!!");
    rl.setPrompt(prefix, prefix.length);
    rl.prompt();
    return 0;
  }
  let dataObj = {};
  let dataArr = line.trim().split(" ");
  [dataObj.type, ...dataObj.args] = dataArr;

  client.write(JSON.stringify(dataObj));

}).on('close', () => {
  console.log('Have a great day!');
  process.exit(0);
});
