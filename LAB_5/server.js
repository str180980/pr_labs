const net = require('net');
const levenshtein = require('fast-levenshtein');

const helpInfo = `
Comenzile suportate:
1) /hello: raspunde cu textul care a fost expediat ca parametru.
2) /help: raspunde cu o lista a comenzilor suportate si o descriere a fiecarei comenzi.
3) /time: returneaza timpul curent.
4) /randomNr: returneaza un numar aleator.
5) /isEven: determina daca un numar este par.
6) /reverseStr: inverseaza un sir de caractere.
`
const comandsObj = {
  help: "/help",
  hello: "/hello",
  time: "/time",
  randomNr: "/randomNr",
  isEven: "/isEven",
  reverseStr: "/reverseStr"
};


const levenshteinUtility = (obj, checkStr) => {
  for (let prop in obj) {
    if(obj.hasOwnProperty(prop)) {
      if(levenshtein.get(obj[prop], checkStr) < 3)
        return `Oops, ${checkStr} is an invalid command. Maybe you've meant ${obj[prop]} ?`;
    }
  }

  return "Ooops! No such command!";
}


const server = net.createServer(socket => {
  socket.setEncoding("utf8");
  socket.write('Echo server\r\n');
  // socket.pipe(socket);
  socket.on("data", (data) => {
    console.log("RECEIVE MESSAGE HERE\n\n\n");
      data = JSON.parse(data);
    const includesRes = Object.values(comandsObj).includes(data.type);
    if(!includesRes) {
      socket.write(levenshteinUtility(comandsObj, data.type));
      return ;
    }

    switch (data.type) {
      case comandsObj.hello:
        console.log("HERE EW ARE !!!!!");
        console.log("START\n\n");
        console.log("END");
        socket.write(data.args.join(" "));
        break;
      case comandsObj.help:
        console.log(helpInfo);

        socket.write(helpInfo);
        break;
      case comandsObj.time:
        const currentDate = new Date(),
          time = `The time is: ${currentDate.getHours()}:${currentDate.getMinutes()}:${currentDate.getSeconds()}`;
        socket.write(time);
        break;
      case comandsObj.randomNr:
        if (data.args < 2 || data.args.length > 2) {
          socket.write("Wrong number of parameters");
        } else {
          const [min, max] = data.args,
            randomNr = Math.floor(Math.random() * (Number(max) - Number(min) + 1)) + Number(min);
          socket.write(randomNr.toString());
        }
        break;
      case comandsObj.isEven:
        if (data.args.length !== 1)
          socket.write("Wrong number of parameters");
        else {
          if (data.args % 2 === 0)
            socket.write("The number is even");
          else
            socket.write("The number is not even");
        }
        break;
      case comandsObj.reverseStr:
        let msg = data.args
          .join(" ")
          .split("")
          .reverse()
          .join("");

        socket.write(msg);
        break;
      default:
        socket.write("Ooops! No such command!");
    }


  })

});

server.listen(1337, '127.0.0.1');
