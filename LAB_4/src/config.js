
module.exports = {
  port: process.env.PORT || 3000,
  env: process.env.NODE_ENV || "development",
  development: {
    db: {
      username: process.env.DATABASE_USER,
      password: process.env.DATABASE_USER_PWD,
      database: process.env.DATABASE,
      host: "localhost",
      dialect: "mysql",
      pool: {
        max: 10,
        min: 0,
        idle: 10000
      }
    }
  }
}
