module.exports = {
  inbox: "INBOX",
  sent: "SENT",
  trash: "TRASH",
  draft: "DRAFT",
  spam: "SPAM",
  unread: "UNREAD"
}
