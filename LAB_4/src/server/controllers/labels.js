"use strict";

const labels = require("../../common/labelsIds");
const {
  google
} = require('googleapis');
const oauth2Client = new google.auth.OAuth2(
  process.env.GOOGLE_CONSUMER_KEY,
  process.env.GOOGLE_SECRET,
  "/login/google/callback");
const gmail = google.gmail('v1');
const _publics = {};



_publics.getLabelsTotal = (user) => {

  credentialsSetUp(user);
  const getLabelsArr = [labels.inbox, labels.draft].map(label =>
    gmail.users.labels.get({
      auth: oauth2Client,
      userId: user.googleId,
      id: label
    }))

  return Promise.all(getLabelsArr)
    .then((label) => ({
      inbox: label[0].data.threadsUnread,
      draft: label[1].data.threadsTotal
    }))
    .catch(err => {
      console.log("\n\n\nApi error " + err);
    })
}



module.exports = _publics;

const credentialsSetUp = (user) => {
  oauth2Client.credentials = {
    access_token: user.googleToken,
    refresh_token: user.googleRefreshTkn
  };
}
