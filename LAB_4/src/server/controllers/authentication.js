"use strict";

const passport = require("passport");
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const models = require("../models");
const User = models.user;
const sequelize = models.sequelize;

passport.use(new GoogleStrategy({
    clientID: process.env.GOOGLE_CONSUMER_KEY,
    clientSecret: process.env.GOOGLE_SECRET,
    callbackURL: "/login/google/callback"
  },
  (token, tokenSecret, profile, done) => {
    console.log("PROFILE!!!!!\n\n\n");
    console.log(token);
    console.log("TOKEN SECRET\n\n\n");
    console.log(tokenSecret);
    User.count({
        where: {
          googleId: profile.id
        }
      })
      .then(count => count > 0 ?
        sequelize.transaction(t => User.update({
          googleToken: token,
          googleRefreshTkn: tokenSecret,
          name: profile.displayName
        }, {
          where: {
            googleId: profile.id
          },
          transaction: t
        })) :
        sequelize.transaction(t => User.create({
          googleId: profile.id,
          googleToken: token,
          googleRefreshTkn: tokenSecret,
          name: profile.displayName,
          email: profile.emails[0].value
        }, {
          transaction: t
        }))
      )
      .then(() => User.findOne({
        where: {
          googleId: profile.id
        },
        attributes: ["id", "email", "googleId", "googleToken", "googleRefreshTkn"]
      }))
      .then(user => user ?
        done(null, user) :
        done(null, false, {
          messages: "USER_NOT_FOUND"
        }))
      .catch(done);
  }
));



passport.serializeUser((user, done) => done(null, user.id));

passport.deserializeUser((id, done) => User.findOne({
    where: {
      id: id
    }
  })
  .then(user => done(null, user))
  .catch(done));

module.exports = passport;
