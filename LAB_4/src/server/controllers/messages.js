"use strict";

const labels = require("../../common/labelsIds");
const {
  google
} = require('googleapis');
const oauth2Client = new google.auth.OAuth2(
  process.env.GOOGLE_CONSUMER_KEY,
  process.env.GOOGLE_SECRET,
  "/login/google/callback");
const gmail = google.gmail('v1');
const _publics = {};

function getMultipleEmailsIds(user, type) {
  console.log("MULTIPLE GET HERE\n\n\n");
  console.log(type);
  return gmail.users.messages.list({
      auth: oauth2Client,
      userId: user.googleId,
      labelIds: labels[type],
      maxResults: 20
    })
    .then(res => ({
      arrayOfIds: res.data.messages.map(({
        id
      }) => id),
      nextPage: res.data.nextPageToken
    }))
}
const getHeaders = (headers, ...headersProp) => {
  let headersObj = {};
  headers.map(({
    name,
    value
  }) => {
    if (headersProp.includes(name))
      headersObj[name] = value;
  });

  return headersObj;
}
const msgMapping = msgArr => msgArr.map(({
  data: {
    id,
    snippet,
    payload
  }
}) => {
  let headersObj = getHeaders(payload.headers, "Subject", "Date", "From");
  console.log("HERE ARE HEADERS\n\n");
  console.log(headersObj);
/*  payload.headers.map(({
    name,
    value
  }) => {
    if (name === "Subject" || name === "Date" || name === "From")
      headersObj[name] = value;
  });*/
  headersObj.snippet = snippet;
  headersObj.id = id;
  return headersObj;
})

_publics.MultipleFetchMsgIds = (user, type) => {
  credentialsSetUp(user);
  return getMultipleEmailsIds(user, type)
    .then(({
      arrayOfIds,
      nextPage
    }) => {

      let getEmailPromises = arrayOfIds.map(id => gmail.users.messages.get({
        auth: oauth2Client,
        userId: user.googleId,
        id: id
      }));
      return Promise.all(getEmailPromises)
        .then(data => msgMapping(data))
        .then(data => ({
          msgArray: data,
          nextPage: nextPage
        }))
    })

}

_publics.getMsgById = (user, id) => {
  credentialsSetUp(user);
  console.log("GET MSG BY ID \n\n");
  console.log(id);
  return gmail.users.messages.get({
    auth: oauth2Client,
    userId: user.googleId,
    id: id
  })
  .then(({data: {payload}}) => {
     let body = null;
    console.log("HERE IS RES\n\n\n");
   console.log(payload);
    let headersObj = getHeaders(payload.headers, "Subject", "Date", "From");
    if(payload.parts) {
      let partsHtml = payload.parts.filter(msg => msg.mimeType === "text/html");
      console.log("DATA HTML HERE\n\n\n");
      console.log(partsHtml);
      body = new Buffer(partsHtml[0].body.data, 'base64').toString("utf8");
    } else body = new Buffer(payload.body.data, 'base64').toString("utf8");

    return {
      headers: headersObj,
      body: body
    }
  //  console.log(body);
  })
}

module.exports = _publics;

const credentialsSetUp = (user) => {
  oauth2Client.credentials = {
    access_token: user.googleToken,
    refresh_token: user.googleRefreshTkn
  };
}
