"use strict";
const path = require("path");
const utils = require("../utils");
const {
  google
} = require('googleapis');
const oauth2Client = new google.auth.OAuth2(
  process.env.GOOGLE_CONSUMER_KEY,
  process.env.GOOGLE_SECRET,
  "/login/google/callback");
const gmail = google.gmail('v1');
const _publics = {};

_publics.sendMail = (user, data, attachArr) => {
    credentialsSetUp(user);
    console.log("HERE IS USER\n\n\n");
    console.log(user);

    let attachmentsArr = attachArr.map(el => ({
      filename: el.originalname,
      content: el.buffer
    }));
    const msgOptions = {
      from: user.email,
      to: data.to,
      subject: data.subject,
      html: data.emailBody,
      attachments: attachmentsArr
    }

    return utils.sendMail(msgOptions, user)
      .then(() => {console.log("EMAIL SUCCESSFULY SEND\n\n\n");})
      .catch(err => {
        console.log("CONTROLLER HERE\n\n\n");
        console.log(err);
      })
}




module.exports = _publics;

const credentialsSetUp = (user) => {
  oauth2Client.credentials = {
    access_token: user.googleToken,
    refresh_token: user.googleRefreshTkn
  };
}
