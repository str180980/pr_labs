"use strict";

const path = require("path");
const nodemailer = require("nodemailer");
const _publics = {};


const verifyTransporter = (transporter) => {
  return new Promise((resolve, reject) => {
    transporter.verify((error, success) => {
      if (error) {
      //  logger(error);
      console.log(error);
        reject(error)
      } else
        resolve(success)
    });
  })
}

/**
 * This function sends an email, before sending the transporter is verified and in case of failure the message is sent again.
 * @returns {Object} returns an object that holds methods for sending and configuring the email
 */

module.exports = (msgOptions, user) => {

  // let transporter = nodemailer.createTransport({
  //   service: "gmail",
  //   auth: {
  //     user: process.env.EMAIL_USER,
  //     pass: process.env.EMAIL_PWD
  //   }
  // });

  let transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
        type: 'OAuth2',
        user: user.email,
        accessToken: user.googleToken
    }
});


  return new Promise((resolve, reject) => {
    verifyTransporter(transporter)
      .then(() => {

        (function emailResendOnError(index) {
          transporter.sendMail(msgOptions, (error, info) => {
            if (error) {
              if (index > 0) {
              //  logger(error);
                reject(error);
                return 0
              }
              emailResendOnError(++index);
            } else resolve(true);
          });
        })(0);
      })
      .catch(reject);
  })
}
