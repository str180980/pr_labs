"use strict";

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define("user", {
    name: {
      type: DataTypes.STRING(20),
      allowNull: false,
      defaultValue: "",
      validate: {
        notEmpty: {
          msg: "USER_NAME_IS_EMPTY"
        }
      }
    },
    email: {
      type: DataTypes.STRING(20),
      allowNull: false,
      defaultValue: "",
      validate: {
        notEmpty: true,
        isUnique(value) {
          return User.count({
            where: { email: value, deletedAt: null}
          }).then(count => {
            if (count > 0) throw new Error("EMAIL_EXISTS");
          });
        }
      }
    },
    googleId: {
      type: DataTypes.STRING
    },
    googleToken: {
      type: DataTypes.STRING
    },
    googleRefreshTkn: {
      type: DataTypes.STRING
    }
  }, {
    paranoid: true,
  });

  return User;
};
