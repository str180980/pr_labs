"use strict";

const router = require("express").Router();
const multer  = require('multer')
const upload = multer();

const labelsController = require("../controllers/labels");
const messageController = require("../controllers/messages");
const  mailer = require("../controllers/sendMail");
const url = require('url');


const {
  google
} = require('googleapis');
const oauth2Client = new google.auth.OAuth2(process.env.GOOGLE_CONSUMER_KEY,
  process.env.GOOGLE_SECRET, "/login/google/callback");


router.use((req, res, next) => {
  if (req.user)
    return next();
  res.redirect("/login")
});

router.get("/", (req, res) => {
  console.log("HERE IS MAIL CLIENT\n\n");
  labelsController.getLabelsTotal(req.user)
    .then(result => {
      res.locals = result;
      res.render("mailClient", res.locals);
    })
});

router.get("/labels/:type", (req, res, next) => {
    console.log("SHOW MSG!!!\n\n");
    console.log(req.params);
    messageController.MultipleFetchMsgIds(req.user, req.params.type)
      .then(msg => {

        res.locals.messages = msg.msgArray;
        console.log("HERE ARE MESSAGES\n\n\n\n\n\n");
        console.log(res.locals.messages);
        res.render("partials/msgTable", res.locals);
      })
});

router.get("/labels/msg/:id", (req, res, next) => {
    return messageController.getMsgById(req.user, req.params.id)
      .then(data => {
        console.log("RESULT ROUTES\n\n\n");
        console.log(data);
        res.json(data)
      })
})

router.post("/sendMail", upload.array('files'), (req, res, next) => {
    console.log("HERE IS BODY\n\n");
    console.log(req.body);
    console.log(req.files);
    return mailer.sendMail(req.user, req.body, req.files)
      .then(data => {
        console.log("BLABLA SEND HERE");
        res.send(true);
      })
});

module.exports = router;
