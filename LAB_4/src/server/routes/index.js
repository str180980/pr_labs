"use strict";

const router = require("express").Router();
const session = require("express-session");
const SequelizeStore = require('connect-session-sequelize')(session.Store);
const passport = require('passport');
const models = require('../models');
const passportHandler = require("../controllers/authentication");
const cookieMaxAge = 24 * 60 * 60 * 1000;

router.use(session({
  secret: "dsafadsfadsf",
  name: "sid",
  maxAge: cookieMaxAge,
  cookie: {
    secure: false,
    path: "/"
  },
  store: new SequelizeStore({
    checkExpirationInterval: 15 * 60 * 1000, // The interval at which to cleanup expired sessions in milliseconds.
    expiration: cookieMaxAge,
    db: models.sequelize
  }),
  saveUninitialized: false,
  resave: false
}));

router.use(passport.initialize());
router.use(passport.session());
router.use("/mailClient", require("./mailClient"))

router.get("/login", (req, res) => {
  res.render("index");
});

router.get("/login/google", passportHandler.authenticate('google', {
  scope: ['https://mail.google.com/', 'email', 'profile'],
  accessType: 'offline',
  prompt: 'consent'
}));

router.get("/login/google/callback", passport.authenticate('google', {
    failureRedirect: '/'
  }),
  function(req, res) {
    res.redirect('/mailClient');
  });


module.exports = router;
