"use strict";

const models = require("./server/models");

module.exports = (app, server) => {
  const sequelizeInitPromise = models.sequelize.sync();

  sequelizeInitPromise
    .then(() => console.log("Connection has been established successfully."))
    .catch(err => {
      server.close();
      console.error(err);
      process.exit(1);
    });
};
