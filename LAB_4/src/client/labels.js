import * as utils from "./utils.js";



export const Labels = (_type) => {
  console.log("WINDOW LOCATION\n\n" + _type);
  const onSucces = (res) => {
    console.log("SUCCESS\n\n\n");
    $("#msgContainer").css({display: "none"});
    $("#tableSection").css({display: "block"});
    $("#tableSection").html(res);
    $("table td div").css({
      width: $(window).width() - 700
    });
  }
  const onError = err => console.log(err);
  const pushStateObj = {
    data: {
      type: "label",
      labelType: _type,
    },
    title: "",
    url: `${window.location.pathname}#${_type}`
  }
  const proto = {
    type: _type,
    getLabel: (onAjaxResolve = true) => {
      return new Promise((resolve, reject) => {
        const options = {
          url: `mailClient/labels/${_type}`,
          method: "GET"
        };
        utils.ajaxRequest(options, onSucces, onError)
          .then(() => {
            console.log("HERE ?????\n\n");
            onAjaxResolve(pushStateObj);
            resolve(true);
          })
          .catch(err => {
            console.log("CATCH HERE?\n\n\n");
            console.log(err);
            reject(false);
          })
      });
    }
  }

  return proto;
}
