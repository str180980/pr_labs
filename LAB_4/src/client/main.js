import * as utils from "./utils.js";
import {
  Labels
} from "./labels.js";

import {
  Message
} from "./messages.js";
import {
  msgSender
} from "./emailSender.js";


(function() {
  msgSender();
  console.log("LOADER HERE\n\n");
  console.log(utils.loaderHtml);
  let file = null,
    formData = null;
  const labels = {
    inbox: Labels("inbox"),
    sent: Labels("sent"),
    draft: Labels("draft")
  }
  $('.modal').modal({
    onCloseEnd: () => {
      console.log("CLOSE MODAL AAAA\n\n\n");
      $("#msgModal input").val("");
      $("#email-body").text("");
      $(".attacmentsContainer").empty();
    }
  });

  $("#sentLabel").click(e => {
    $(".loader").html(utils.loaderHtml);
    labels.sent.getLabel(utils.pushState)
      .then(() => $(".loader").empty());

  });draftLabel

  $("#inboxLabel").click(e => {
    $(".loader").html(utils.loaderHtml);
    labels.inbox.getLabel(utils.pushState)
      .then(() => {
        console.log("IM HERE\n\n");
        $(".loader").empty();
      });

  });
  $("#draftLabel").click(e => {
    $(".loader").html(utils.loaderHtml);
    labels.draft.getLabel(utils.pushState)
      .then(() => {
        console.log("IM HERE\n\n");
        $(".loader").empty();
      });

  });


  $("body")
    .on("click", "table tr", e => {
      let id = $(e.currentTarget).attr("id");
      let msg = Message(id);
      msg.getMsg(utils.pushState);

    });



  $(window).on("popstate", e => {
    let state = e.originalEvent.state;
    let _pushState = utils.pushState;
    if (state === null) {
      state = {
        type: "label",
        labelType: window.location.hash.substr(1),
      }
    }
    console.log("STATE HERE\n\n\n\n");
    console.log(state);
    switch (state.type) {
      case "label":
        labels[state.labelType].getLabel();
        break;
      case "message":
        Message(state.msgId).getMsg();
        break;
      default:

    }
  });

  $(window).on("resize", e => {
    let tableBodyWidth = $(window).width() - 700;
    $("table td div").css({
      width: tableBodyWidth
    });
  })


})();
