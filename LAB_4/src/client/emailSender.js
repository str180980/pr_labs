  import * as utils from "./utils.js";



  let formData = null;
  let file = null;
  const ccEmail = `
  <div class="input-field col s12">
    <input id="ccEmail" placeholder="To" id="toEmail" type="text" required>
    <label for="ccEmail" style="width:100%; background-color:">Cc</label>

  </div>
  `;


  const sendMail = e => {
      formData = null;
      file = null;
    $(".loader").html(utils.loaderHtml);

    if (!formData || !file) {
      console.log("FORMA DATA HERE\n\n\n\n\n");
      formData = new FormData();
    }
    console.log("COOOOL\n\n\n");
    let emailData = {
      to: $("#to-email").val(),
      cc: $("#ccEmai").val(),
      subject: $("#email-subject").val(),
      emailBody: $("#email-body").html()
    }
    if (emailData.cc) {
      console.log("HERE IS CC\n\n");
      console.log(emailData.cc);
    } else
      formData.append("to", emailData.to);
    formData.append("subject", emailData.subject);
    formData.append("emailBody", emailData.emailBody);
    console.log("EMAIL BODY\n\n");
    console.log(emailData.emailBody);
    if (emailData.to && emailData.subject && emailData.emailBody) {

      e.preventDefault();
      console.log("URL HERE\n\n");
      console.log(window.location.pathname);
      //  console.log(emailData);
      //  emailData = JSON.stringify(emailData);
      const options = {
        url: `/mailClient/sendMail`,
        type: "POST",
        dataType: "json",
        data: formData,
        processData: false,
        contentType: false,
        cache: false
      }
      console.log("HERE IS BODY\n\n");
      console.log(emailData.subject);
      utils.ajaxRequest(options, data => {
          $(".modal").modal("close");
        //  $(".loader").empty()
          M.toast({
            html: 'Email successfuly sent!'
          });
        },
        () => console.log("ERRROR \n\n\n"));
    }
  }

  const attachFiles = e => {
   file = $('#videoUpload')[0].files[0];
    //  filesArr.push(file);
    const attachmentsName = `<a id="${file.name}" class="collection-item"></a>`;
    const attachmentSize = `<span id="${file.name}Size" style="color: grey"></span>`;
    console.log("FILE HERE\n\n");
    console.log(file);
    if (!formData) {
      console.log("FORMA DATA HERE\n\n\n\n\n");
      formData = new FormData();
    }

    formData.append("files", file, file.name);


    $(".attacmentsContainer").append(attachmentsName);
    $(".attacmentsContainer a:last-child").text(`${file.name}`);
    $(".attacmentsContainer a:last-child").append(attachmentSize);
    $(".attacmentsContainer a:last-child span").text(`(${Math.round(file.size / 1024)}kb)`);
    // $("#fileAttachName").text(`${file.name}`);
    // $("#fileAttachSize").text(`${file.size}`);
    console.log("FOMR DATA HERE\n\n");
    console.log(formData);
  }

  export const msgSender = () => {

    $("#sendMailBtn").on("click", sendMail);
    $('input[type="file"]').on("change", attachFiles);

    $("#ccMails").on("click", e => {
      console.log("CC MAILS HERE\n\n\n");
      $("#modalFields").prepend(ccEmail);
    })

  }
