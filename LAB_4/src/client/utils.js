
export  const loaderHtml = `
  <div class = "spinner-layer spinner-red-only">
     <div class = "circle-clipper left">
        <div class = "circle"></div>
     </div>

     <div class = "gap-patch">
        <div class = "circle"></div>
     </div>

     <div class = "circle-clipper right">
        <div class = "circle"></div>
     </div>
  </div>
  `;

export const ajaxRequest = ({
  url,
  data,
  type = "GET",
  dataType = "html",
  processData = true,
  contentType = "text/html",
  cache = true
}, onSuccess, onError) => {

  //  if(!(url && data))
  //  return ;

  console.log("HERE WE ARE");
  const settings = {};
  settings.url = url;
  settings.type = type;
  settings.dataType = dataType;

  if (data)
     settings.data = data;

  if(!processData && !contentType && !cache) {
    settings.processData = processData;
    settings.contentType = contentType;
    settings.cache = cache;
  }

  return new Promise((resolve, reject) => {
    $.ajax(settings)
      .done(res => {
        onSuccess(res);
        resolve(true);
      })
      .fail(err => {
        onError(err);
        reject(err);
      })
  })

}

export const pushState = ({data, title, url}) => {

  window.history.pushState(data, title, url);
  console.log("AFTER PUSH STATE");
  console.log(window.location.pathname + window.location.hash);
}
