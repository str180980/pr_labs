import * as utils from "./utils.js";

export const Message = (_id) => {
  let onSucces = data => {

    let fromData = decodeURIComponent(data.headers.From).replace(/[<>]/g, "'");
    $("#tableSection").css({
      display: "none"
    });
    $("#msgContainer").css({display: "block"});
    $("#msgSubject").html(data.headers.Subject);
    $("#msgFrom").html(fromData);
    $("#msgDate").html(data.headers.Date);
    $(".msgBody").html(data.body);
  }
  let onError = err => console.log(err);

  const proto = {
    id: _id,
    getMsg: (onAjaxResolve = true) => {
      return new Promise((resolve, reject) => {
        let options = {
          url: `/mailClient/labels/msg/${_id}`,
          method: "GET",
          dataType: "json"
        }
        const pushStateObj = {
          data: {
            type: "message",
            msgId: _id
          },
          title: "",
          url: `${window.location.pathname}${window.location.hash}/${_id}`
        }
        utils.ajaxRequest(options, onSucces, onError)
          .then(() => {
            onAjaxResolve(pushStateObj);
            resolve(true);
          })
          .catch(err => {
            console.log(err);
            reject(false);
          })
      });
    }
  }

  return proto;
}
