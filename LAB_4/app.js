
require('dotenv').config();
const express = require('express');
const dbSync = require('./src/databaseInit');
const app = express();


app.set("view engine", "ejs");
app.set("views", "./src/server/views");

app.use(express.static("./public"));
app.use(express.json());
app.use(express.urlencoded({ extended: false}));

app.use("/", require("./src/server/routes"));


const server = app.listen(3000, err => {
  if (err) {
    console.log(err);
    server.close();
  }
});

if (server.listening) {
  console.log("LISTENING~~~~\n\n\n");
  dbSync(app, server);
}

module.exports = app;
