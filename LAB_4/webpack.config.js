"use strict";

/**
 * Webpack config
 * @author LEBO Projects
 * @type {webpack}
 */

const webpack = require("webpack");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const debug = process.env.NODE_ENV !== "production";

module.exports = {
  entry: {
    main: "./src/client/main.js",
  },
  output: {
    path: __dirname + "/public/js",
    filename: "[name].min.js"
  },
  module: {
  rules: [
    {
      test: /\.js$/,
      exclude: /(node_modules)/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: ["es2015"]
        }
      }
    }
  ]
}
};
