function totalPerCategory(data) {
  let categories = [];
  let check;

  data.map((elem) => {
    let categoryObject = {};
    check = categories.filter((catElem) => catElem.id === elem.category_id);
    // console.log(check);
    if (check[0]) {
      categories.forEach((catEll) => {
        if (catEll.id === elem.category_id)
          catEll.sum += parseInt(elem.total);
      });
    } else {

      categoryObject.id = elem.category_id;
      categoryObject.sum = parseInt(elem.total);
      categories.push(categoryObject);
    }
  });

  return categories;
}

onmessage = (event) => {

  data = event.data;
  let totalPerCat = totalPerCategory(data);
  postMessage(totalPerCat);
  self.close();
};
