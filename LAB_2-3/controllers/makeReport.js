    const fetch = require('node-fetch');
    const Threads = require('webworker-threads');

    module.exports.createReport = (formInputs, response) => {
      let validCategoryCollums = ["id", "name", "category_id"],
        validOrderCollums = ["created", "total", "id", "category_id"],
        start = formInputs.fromDate,
        end = formInputs.toDate,
        categoryURL = "https://evil-legacy-service.herokuapp.com/api/v101/categories/",
        ordersURL = `https://evil-legacy-service.herokuapp.com/api/v101/orders/?start=${start}&end=${end}`,
        headers = {
          headers: {
            "Accept": "text/csv",
            "x-api-key": "55193451-1409-4729-9cd4-7c65d63b8e76"
          }
        }

      Promise.all([
          fetch(categoryURL, headers).then(res => checkHeader(res))
        , fetch(ordersURL, headers).then(res => checkHeader(res))
        ])
        .then(results => {

          let categories = csvJSON(results[0], validCategoryCollums, response);
          let orders = csvJSON(results[1], validOrderCollums, response);
          let worker = new Threads.Worker(__dirname + "/../worker.js");
          let categObj = makeRecursiveCategoryArray(categories);;

          worker.postMessage(orders);

          worker.addEventListener("message", (event) => {

            assignTotalPerCategories(categObj, event.data);

            response.render("index", {
              data: categObj
            })
          })
        })
        .catch(err => {
          console.log(err);
        })
    }

    function checkHeader(res) {
      if (res.headers.get("content-type").indexOf("text/csv") !== -1) { // checking response header
        return res.text();
      } else {
        throw new TypeError('Response from "' + url + '" has unexpected "content-type"');
      }
    }

    function assignTotalPerCategories(categObj, orderObj) {

      let order;
      let sum = 0;
      let categSum;
      if (!categObj[0]) {

        return 0;
      }

      categObj.forEach((elem) => {

        order = orderObj.filter((elemOrder) => elemOrder.id === elem.id);
        if (!order[0])
          order = [{
            sum: 0
          }];

        elem.total = order[0].sum;

        /****** If not counting parent orders*********/
        if (elem.childs[0]) {
          elem.total = 0;
        }
        /********************************************/
        categSum = assignTotalPerCategories(elem.childs, orderObj);
        elem.total = categSum + elem.total;
        sum += elem.total;
        return elem.total;
      });

      return sum;
    }

    function csvJSON(csv, validCollums, response) {

      var lines = csv.split("\n");
      var result = [];
      var headers = lines[0].split(",");
      headers.forEach((elem, index) => {;
        if (validCollums.indexOf(elem) === -1) {
          response.send("INVALID COLLUMS");
        }

      })
      for (var i = 1; i < lines.length - 1; i++) {
        var obj = {};
        var currentline = lines[i].split(",");
        for (var j = 0; j < headers.length; j++) {
          obj[headers[j]] = currentline[j];
        }
        result.push(obj);
      }

      return result; //JSON
    }

    function createSubCategoryArray(info, parent, obj) {
      let childs = [];

      if (!parent)
        return true;

      return info.filter((Elem) => Elem.category_id === parent.id)
        .forEach((resElem, index) => {
          let childObj = {};
          childObj.name = resElem.name;
          childObj.id = resElem.id;
          childObj.total = null;
          childObj.childs = [];
          obj.childs.push(childObj);
          createSubCategoryArray(info, resElem, obj.childs[index]);
        });
    }

    function makeRecursiveCategoryArray(info) {
      let categoriesObj = [];
      let ind = 0;
      info.map((elem, index) => {
        if (!elem.category_id) {
          let category = {};
          category.name = elem.name;
          category.id = elem.id;
          category.total = null;
          category.childs = [];
          categoriesObj.push(category);

          createSubCategoryArray(info, elem, categoriesObj[ind]);
          ind++;
        }

      });

      return categoriesObj;
    }
