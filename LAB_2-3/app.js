    const express = require('express');
    const fetch = require('node-fetch');
    const bodyParser = require('body-parser')
    const Threads = require('webworker-threads');
    const reportController = require('./controllers/makeReport');
    const app = express();

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ // to support URL-encoded bodies
      extended: true
    }));
    app.set('view engine', 'ejs');
    app.use(express.static(__dirname + '/public'));

    app.get('/', (req, res) => {
      res.render("index", {
        data: null
      })
    });

    app.post('/generate', (req, res) => {
      reportController.createReport(req.body, res);
    });

    app.listen(3000, () => {
      console.log('Connected to port 3000');
    });
